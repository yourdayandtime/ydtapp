import Link from "next/link";
import Layout from "../components/layout";
import Announcement from "../components/announcement";
export default function Home() {
  return (
    <Layout>
      <div className="row">
        <div className="col-md-10 mx-auto">
          <h1 className="title">Your Day and Time</h1>
          <div className="card">
            <div className="card-body">
              <blockquote className="description blockquote">
                Time management is very important, we need it in our lives.
                There are so many things we are busy with, yet we are running
                out of time. We are here to help refugees to know in advance
                what is the day and time of the next appointment for anything
                when dealing with Home Affairs, especially for permit renewals.
                By doing so, we assist you, free of charge, in managing well your time and avoid
                queuing without being served.
                <span className="ml-1 btn btn-secondary btn-sm">See how this works</span>
              </blockquote>
            </div>
          </div>
          <div className="grid">
            <Announcement />
            <div className="text-center  mb-3 mt-3">
              <p>Click Join Now if you haven't joined.</p>
              <button className="btn btn-lg btn-outline-info bg-info rounded-pill">
                <Link href="/join">
                  <a>Join Now</a>
                </Link>
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
