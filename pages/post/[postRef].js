import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import parseHtml from "html-react-parser";
import Layout from "../../components/layout";
const Page = () => {
  const [post, setPost] = useState(null);
  const router = useRouter();
  const { postRef } = router.query;
  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/page/findByField/ref/${postRef}`)
      .then((p) => {
        if (p.data) {
          setPost(p.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, [postRef, setPost]);
  if (!post) return <div>Loading...</div>;
  return (
    <Layout>
      <div className="row">
        <div className="col-md-12 mx-auto">
          <h1 className="header">{post.title}</h1>
          {parseHtml(post.text)}
        </div>
      </div>
    </Layout>
  );
};

export default Page;
