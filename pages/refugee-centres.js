import Layout from "../components/layout";
import RefugeeCenters from "../components/refugeeCenters";
export default function Centers() {
  return (
    <Layout>
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="mt-1 mt-2"></div>
            <h1 className="title mx-auto">
              Home Affairs Refugee Centres
            </h1>
            <div className="grid">
            <RefugeeCenters/>
          </div>
          </div>
        </div>
    </Layout>
  );
}
