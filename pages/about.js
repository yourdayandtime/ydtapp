import Layout from "../components/layout";
import LogoText from "../components/utilities/logoText";
export default function About() {
  return (
    <Layout>
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="mt-1 mt-2"></div>
            <h1 className="title mx-auto">
              About us
            </h1>
            <div className="grid">
            <div className="card mt-1">
              <div className="card-body justify-content-center">
                <p>
                  The current mission of Your Day and Time (<LogoText/>) is to assist refugees and asylum seekers in South Africa with quick access to instant information related to appointments, applications follow ups, permits renewal and general annoucements published by the refugee center.  
                </p>
                <p>
                  Helping Home Affairs officials, refugees and asylum seekers is an important matter these days. We assist, through YDT platform, by automating information flow between the parties more quicker and efficient.
                </p>
                <p>
                  Please do not hesitate to contact  us, about any information or query, by sending an email to <span className='text-primary'>info@yourdayandtime.co.za</span>.
                </p>
                <p>
                  For a quick and direct contact, use <a href="#">this form</a> or call 021 3102 111 
                </p>
                <p>
                  You may as well be interested in reading our regular on <a href='#'>Announcements</a> or visit our <a href='/faq'>FAQ</a> page.
                </p>
                <p>We value your trust.</p>
                <p className="text-muted">The { " " } <LogoText/> {" "}Team</p>
              </div>
            </div>
          </div>
          </div>
        </div>
    </Layout>
  );
}
