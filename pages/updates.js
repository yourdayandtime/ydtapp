import Head from "next/head";
import Layout from "../components/layout";
import AnnUpdates from "../components/announcements"
export default function Updates() {
  return (
    <Layout>
      <Head>
        <title>YDT - Updates</title>
      </Head>
      <h1>Updates</h1>
      <AnnUpdates/>
    </Layout>
  );
}
