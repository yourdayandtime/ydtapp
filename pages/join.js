import Head from "next/head";
import SignUpForm from "../components/singup";
import Layout from "../components/layout";
export default function Join() {
  return (
    <Layout>
      <Head>
        <title>Join</title>
      </Head>
      <h1>Join - YDT</h1>
      <SignUpForm/>
    </Layout>
  );
}
