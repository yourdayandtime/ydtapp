import Layout from "../components/layout";
export default function Privacy() {
  return (
    <Layout>
      <div className="row">
        <div className="col-md-10 mx-auto">
          <h1 className="title mx-auto">Privacy Policy</h1>
          <div className="grid">
            <div className="card mt-1">
              <div className="card-body justify-content-center small font-italic">
                <p>Privacy content to go here.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}