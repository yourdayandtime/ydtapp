import Layout from "../components/layout";
import QuestionsAnswers from "../components/questions";
export default function FAQ() {
  return (
    <Layout>
      <div className="row">
        <div className="col-md-8 mx-auto">
          <div className="mt-1 mt-2"></div>
          <h1 className="title mx-auto">Frequently Asked Questions</h1>
          <div className="card mt-1" id="frequent-questions">
            <div className="card-body">
              <QuestionsAnswers/>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
