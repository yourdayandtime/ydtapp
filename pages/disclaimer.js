import Link from "next/link";
import Layout from "../components/layout";
export default function Disclaimer() {
  return (
    <Layout>
      <div className="row">
        <div className="col-md-10 mx-auto">
          <h1 className="title mx-auto">Legal Disclaimer</h1>
          <div className="grid">
            <div className="card mt-1">
              <div className="card-body justify-content-center">
                <p>
                  YDT has made every attempt to ensure the accuracy and
                  reliability of the information provided on this website.
                </p>
                <p>
                  However, the information is provided “as is” without warranty
                  of any kind. YDT does not accept any responsibility or
                  liability for the accuracy, content, completeness, legality,
                  or reliability of the information contained on this website
                  and information you submit to get home affaires help you.
                </p>
                
              </div>
              </div>
              <div><p className='alert alert-danger'>
                  Remember, every time your permit get renewed, you will need to
                  add the new expiration date{" "}
                  <Link href="/profile">
                    <a>here</a>
                  </Link>
                  . We are working on making this update autmated in the near future.
                </p></div>
            
          </div>
        </div>
      </div>
    </Layout>
  );
}
