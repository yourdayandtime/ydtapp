import Link from "next/link";
import Layout from "../components/layout";
export default function Home() {
  return (
    <Layout>
      <div className="row">
        <div className="col-md-8 mx-auto">
          <div className="mt-1 mt-2"></div>
          <h1 className="title mx-auto">Contact us</h1>
          <div className="grid">
            <div className="card mt-1">
              <div className="card-body justify-content-center">
                <p>
                  Helping Home Affairs and refugees, asylum seekers is crucial. We assit both parties through YDT platform as a tool for successfull communication.
                </p>
                <p>
                  Please do not hesitate to contact us by sending an emal to <Link href='#'><a>info@yourdayandtime.co.za</a></Link> about any information or query.
                </p>
                <p>
                  Due to a high volume of emails, an individual response to each
                  email may be delayed or not possible.
                </p>
                <p>
                  However, we regularly publish list of frequently asked questions you can access on our <Link href='/faq'><a>FAQ page</a></Link>.
                  Please visit that page for regular questions updates.
                </p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </Layout>
  );
}
