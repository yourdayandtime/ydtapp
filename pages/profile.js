import Head from "next/head";
import Profile from "../components/refugeeProfile";
import Layout from "../components/layout";
export default function Join() {
  return (
    <Layout>
      <Head>
        <title>User - Details</title>
      </Head>
      <Profile />
    </Layout>
  );
}
