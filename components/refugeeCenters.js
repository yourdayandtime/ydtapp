import React from "react";
import axios from "axios";
import parseHtml from 'html-react-parser';
class RefugeeCenters extends React.Component {
  constructor(props) {
    super(props);
    this.state = { centers: [], error: 0 };
  }

  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/content/centers`)
      .then((cs) => {
        this.setState({ centers: cs.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentWillUnmount() {}

  render() {
    if (!this.state.centers.length) {
      return <div />;
    }
    var html = "";
    html = this.state.centers.map((c, k) => (
      <div className="card" key={k}>
        <div className={"card-header bg-"+ c.color_cls +" font-weight-bold text-white"}>
          {c.name}
        </div>
        <div className="card-body">{c.text ? parseHtml(c.text): ''}</div>
      </div>
    ));
    return <div className="flex">{html}</div>;
  }
}

export default RefugeeCenters;
