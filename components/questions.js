import React from "react";
import axios from "axios";
import Question from "../components/question";
class Questions extends React.Component {
  constructor(props) {
    super(props);
    this.state = { questions: [], error: 0, showAnswer: false };
    this.toggleShow = this.toggleShow.bind(this);
  }
  toggleShow = () => {
    this.setState({ showAnswer: !this.state.showAnswer });
  };
  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/content/questions`)
      .then((qs) => {
        this.setState({ questions: qs.data.rows });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentWillUnmount() {}

  render() {
    if (!this.state.questions.length) {
      return <div />;
    }
    var html = "";
    html = this.state.questions.map((q, k) => (
      <Question key={k} question={q}/>
    ));
    return <ul className="list-group">{html}</ul>;
  }
}

export default Questions;
