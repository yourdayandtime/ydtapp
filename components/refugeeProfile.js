import React from "react";
import axios from "axios";
import UserProfileField from "../components/profileField";
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profile: false };
  }

  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .post(`${API_URL}/user/getProfile`, { phone: "0718048298" })
      .then((res) => {
        //kind of session must be set here
        if (res.data) {
          this.setState({ profile: res.data });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    if (!this.state.profile) {
      return <div>No details found.</div>;
    }
    let user = this.state.profile;
    let keys = Object.keys(user);
    let html = keys.map((v,k) => (
      <UserProfileField key={k} fieldName={v}  fieldValue={user[v]} user={user} />
    ));
    return <div><h1>Profile</h1><div><p>It's important to make sure all the information is correct and up to date in order to get help. Keep Phone number, expiration date up to date all the time.</p>
    <ol className="font-italic alert alert-info">
      <li className='ml-3'>Update expiration date everytime you renew your permit</li>
      <li  className='ml-3'>Update phone number everytime you no longer use the old number.</li>
    </ol></div><ul className="list-group">{html}</ul></div>;
  }
}
export default Profile;
