import React from "react";
import parseHtml from 'html-react-parser';
class Question extends React.Component {
  constructor(props) {
    super(props);
    this.state = { toggle: false };
  }
  render() {
      const q = this.props.question;
    return (
      <div>
        <li
          className="list-group-item mb-3 bg-danger text-white"
          data-toggle="collapse"
          href={"#collapse-" + q.id}
          role="button"
          aria-expanded="false"
          aria-controls={"collapse-" + q.id}
        >
          <div className="row">
            <div className="col" onClick={() => this.setState({toggle:!this.state.toggle})}>
              {parseHtml(q.question)}
            </div>
            {this.state.toggle ? (
              <i
                className="fa fa-chevron-up float-right mr-2"
                onClick={() => this.setState({toggle:!this.state.toggle})}
              ></i>
            ) : (
              <i
                className="fa fa-chevron-down float-right mr-2"
                onClick={() => this.setState({toggle:!this.state.toggle})}
              ></i>
            )}
          </div>
        </li>
        <div className="collapse p-2" id={"collapse-" + q.id}>
          <div className="card">
            <div className="card-body">{parseHtml(q.answer ? q.answer : '')}</div>
          </div>
        </div>
      </div>
    );
  }
}
export default Question;
