import React from "react";
import axios from "axios";
const $ = require("jquery");
$.DataTable = require("datatables.net");
/* require( 'jszip' );
require( 'pdfmake' ); */
require("datatables.net-buttons-dt");
//require("datatables.net-buttons/js/buttons.colVis.js");
//require("datatables.net-buttons/js/buttons.flash.js");
require("datatables.net-buttons/js/buttons.print.js");
require("datatables.net-buttons/js/buttons.html5.js");
const columns = [
  { title: "Name", data: "fname" },
  { title: "Surname", data: "lname" },
  { title: "Gender", data: "genderString" },
  { title: "Permit type", data: "permit_type" },
  { title: "Permit no", data: "permit_no" },
  { title: "Exp date", data: "exp_date" },
  { title: "Renew", data: "exp_status" },
  { title: "Country", data: "country" },
];
class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], error: 0 };
  }
  componentDidMount() {
    //To do: find way to avoid multiple requests of same data from the server/api and reuse previously received data
    axios
      .get(`${process.env.API_URL}/user/`)
      .then((users) => {
        var table = $("#refugees_table").DataTable({
          //dom: "Bfrtip",//required to display
          dom: 'B<"clear">lfrtip', // this allows to show entries select box to display when other buttons dispay
          data: users.data,
          columns,
          stateSave: true, //keep current data on reload
          buttons: ["print", "excel", "pdf"],
        });
        table.$(':contains("this week")').addClass("bg-success text-white");
        table.$(':contains("next week")').addClass("bg-info text-white");
        table.$(':contains("this month")').addClass("bg-warning text-white");
        table.$(':contains("expired")').addClass("bg-danger text-white");
        table.$(':contains("later")').addClass("bg-primary text-white");

        this.setState({ data: users.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentWillUnmount() {
    $("#refugees_table_wrapper").find("table").DataTable().destroy(true);
  }
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return <table id="refugees_table" className="display table" />;
  }
}

export default DataTable;
