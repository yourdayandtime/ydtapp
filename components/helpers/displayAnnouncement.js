import React from "react";
import parseHtml from "html-react-parser";
import DiplayDateHelper from "./displayDate";
export default class Announcement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.value.title,
      text: this.props.value.text,
    };
  }
 render() {
    
    return (
      <div className="card">
        <div className="card-body">
          <div className="announcement-title mb-2">
            <b>{parseHtml(this.state.title)}</b>
          </div>
          <div className="small mutted mb-2">
            <DiplayDateHelper date={this.props.value.updated_at} />
          </div>
          <div className="announcement-text">{parseHtml(this.state.text)}</div>
        </div>
      </div>
    );
  }
}
