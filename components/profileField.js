import React from "react";
import axios from "axios";
import validator from "validator";
import Country from "./utilities/selectCountry";
import countryIsoName from "./utilities/countryIsoName";
class ProfileField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fieldName: this.props.fieldName,
      fieldValue: this.props.fieldValue,
      error: false,
      success: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.isvalid = this.isvalid.bind(this);
    this.displayError = this.displayError.bind(this);
    this.clearError = this.clearError.bind(this);
    this.isAvailable = this.isAvailable.bind(this);
    this.setError = this.setError.bind(this);
    this.fields = this.fields.bind(this);
  }

  setError = () => {
    if (this.errorMsgs.hasOwnProperty(this.state.fieldName)) {
      var err = this.errorMsgs.hasOwnProperty(this.state.fieldName);
      this.setState({ error: err });
    }
  };
  displayError = () => {
    if (this.state.error) {
      return (
        <span className="text-danger pl-1 small alert">{this.state.error}</span>
      );
    }
  };

  clearError = () => {
    this.setState({ error: false });
  };

  isAvailable = (field, val) => {
    let API_URL = process.env.API_URL;
    axios
      .post(`${API_URL}/user/isavailable`, { field: field, value: val })
      .then((res) => {
        if (!res.data) {
          this.setState({ error: "This has been registered here already." });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  initialErrors = {
    email: "Email format is incorrect",
    phone: "Phone number invalid for SA.",
    permit_no: "Permit number invalid.",
  };
  errorMsgs = {
    email: "Email format is incorrect",
    phone: "Phone number invalid for SA.",
    permit_no: "Permit number invalid.",
    exp_date: "Only permits expiring from 01/01/2020 are allowed.",
    fname: "Please use the first name on your permit with letters only.",
    lname: "Please use the surname on your permit with letters only.",
  };

  isvalid = (e) => {
    var name = e.target.name;
    var value =
      e.target.type === "checkbox" ? e.target.checked : e.target.value;
    this.clearError(name);
    //if (validator.isBoolean(value) && value === true) return;
    if (e.target.type !== "checkbox" && validator.isEmpty(value)) return; //isEmpty function expect strings
    if (name === "email") {
      if (!validator.isEmail(value)) this.setError();
      else this.isAvailable(name, value);
    } else if (name === "phone") {
      if (!validator.isMobilePhone(value, "en-ZA")) this.setError();
      else this.isAvailable(name, value);
    } else if (name === "permit_no") {
      if (!validator.isLength(value, { min: 1, max: 16 })) this.setError();
      else this.isAvailable(name, value);
    } else if (name === "fname" && !validator.isAlpha(value)) {
      this.setError();
    } else if (name === "lname" && !validator.isAlpha(value)) {
      this.setError();
    } else if (name === "exp_date" && !validator.isAfter(value, "2020-01-01")) {
      this.setError();
    }
  };

  handleChange = (e) => {
    var target = e.target;
    var name = target.name;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ fieldValue: value }, this.isvalid(e));
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (validator.isEmpty(this.state.fieldValue))
      this.setState({
        error: "Can't be empty",
      });
    if (this.state.error) {
      return;
    }
    let API_URL = process.env.API_URL;
    var dataUpdate = {
      id: this.props.user.id,
      [this.state.fieldName]:this.state.fieldValue
    };
    if (this.props.fieldValue === this.state.fieldValue) {
      this.setState({ edit: false });
      return;
    }
    axios
      .put(`${API_URL}/user/update`, dataUpdate)
      .then((user) => {
        if (user.data) {
          this.setState({ success: true, edit: false });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  fields = () => {
    return {
      fname: {
        label: "First Name",
        input: () => {
          return (
            <input
              type="text"
              name="fname"
              value={this.state.fieldValue}
              onChange={this.handleChange}
              className="form-control"
              placeholder="First name"
              required={true}
              id="firstname"
              maxLength="50"
            />
          );
        },
      },
      lname: {
        label: "Last Name",
        input: () => {
          return (
            <input
              type="text"
              name="lname"
              value={this.state.fieldValue}
              onChange={this.handleChange}
              className="form-control"
              placeholder="Last name"
              required={true}
              id="lname"
              maxLength="50"
            />
          );
        },
      },
      gender: {
        label: "Gender",
        input: () => {
          return (
            <select
              name="gender"
              value={this.state.gender}
              onChange={this.handleChange}
              className="form-control"
              required={true}
              id="gender"
            >
              <option>Select</option>
              <option value="f">Female</option>
              <option value="m">Male</option>
            </select>
          );
        },
      },
      birth_year: {
        label: "Birth Range",
        input: () => {
          return (
            <select
              name={this.state.fieldName}
              value={this.state.fieldValue}
              onChange={this.handleChange}
              required={true}
              className="form-control"
              id="birth_year"
            >
              <option>Select</option>
              <option value="1950-1960">1950-1960</option>
              <option value="1960-1970">1961-1970</option>
              <option value="1971-1980">1971-1980</option>
              <option value="1981-1990">1981-1990</option>
              <option value="1991-2000">1991-2000</option>
              <option value="2001-2020">2001-2020</option>
            </select>
          );
        },
      },
      refugee_center: {
        label: "Center",
        input: () => {
          return (
            <select
              name={this.state.fieldName}
              value={this.state.fieldValue}
              onChange={this.handleChange}
              className="form-control"
              id="center"
              required={true}
            >
              <option>Select</option>
              <option value="Cape Town">Cape Town</option>
              <option value="Durban">Durban</option>
              <option value="Musina">Musina</option>
              <option value="Pretoria">Pretoria</option>
              <option value="Port Elizabeth">Port Elizabeth</option>
            </select>
          );
        },
      },
      permit_no: {
        label: "Permit Number",
        input: () => {
          return (
            <input
              type="text"
              name="permit_no"
              value={this.state.fieldValue}
              onChange={this.handleChange}
              className="form-control"
              id="permit_no"
              placeholder="Permit Number"
              required={true}
            />
          );
        },
      },
      permit_type: {
        label: "Permit Type",
        input: () => {
          return (
            <select
              name="permit_type"
              value={this.state.fieldValue}
              onChange={this.handleChange}
              className="form-control"
              id="permit_type"
              required={true}
            >
              <option>Select</option>
              <option value="status">Status</option>
              <option value="asylum">Asylum</option>
            </select>
          );
        },
      },
      exp_date: {
        label: "Expiring Date",
        input: () => {
          return (
            <input
              type="date"
              name="exp_date"
              value={this.state.fieldValue}
              onChange={this.handleChange}
              className="form-control"
              id="exp_date"
              required={true}
              placeholder="Permit expiration date"
            />
          );
        },
      },
      phone: {
        label: "Phone",
        input: () => {
          return (
            <input
              type="number"
              className="form-control"
              name="phone"
              value={this.state.fieldValue}
              onChange={this.handleChange}
              id="phone"
              placeholder="Phone Number"
              required={true}
            />
          );
        },
      },
      email: {
        label: "Email Address",
        input: () => {
          return (
            <input
              type="email"
              name="email"
              value={this.state.fieldValue}
              onChange={this.handleChange}
              id="email"
              className="form-control"
              maxLength="50"
            />
          );
        },
      },
      country_iso: {
        label: "Country of Origin",
        input: () => {
          return (
            <Country state={this.state} handleChange={this.handleChange} />
          );
        },
      },
    };
  };

  render() {
     var state = this.state;
    if (!this.fields()[state.fieldName]) return <div />;
    var input = this.fields()[state.fieldName]["input"];
    var fieldValue = state.fieldValue;
    if(state.fieldName === 'gender') {
      fieldValue = state.fieldValue === 'f' ? 'Female' : 'Male';
    } else if (state.fieldName === 'country_iso') {
      fieldValue = countryIsoName()[state.fieldValue];
    }
    return (
      <li className="list-group-item profile-item">
        <div className="row">
          <div className="col-sm font-weight-bold bg-danger text-white p-2">
            <span className='float-right-item'>{this.fields()[state.fieldName]["label"]}
            {" : "}</span>
          </div>
          <div className="col-sm text-white p-2 field-value">
            {state.edit ? (
              <div>
                {input()}
                <i
                  className="fa fa-save mr-2 mt-2"
                  onClick={this.handleSubmit}
                ></i>
                <i
                  className="fa fa-close ml-2 mt-2"
                  onClick={() => {
                    this.setState({ edit: !state.edit });
                  }}
                ></i>
              </div>
            ) : (
              <div>
                <span className="">{fieldValue}</span>
                <small>
                  <i
                    className="fa fa-pencil field-value ml-2 float-right"
                    onClick={() => {
                      this.setState({ edit: true });
                    }}
                  ></i>
                </small>
              </div>
            )}
          </div>
        </div>
      </li>
    );
  }
}
export default ProfileField;
