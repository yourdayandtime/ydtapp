import React, { Component } from "react";
import axios from "axios";
import DispalyAnnouncement from "./helpers/displayAnnouncement";
class Announcements extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anns: [],
      loading:true,
      error: false
    };
  }

  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/content/announcements`)
      .then((result) => {
        if (result.data) {
          this.setState({ anns: result.data, loading: false});
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({error: true});
      });
  }

  render() {
    var annHtml = "";
    if(this.state.loading){
      return <span>Loading ...</span>;
    }
    if(this.state.error) {
      return <span className='text-danger'>Ops! Soomething went wrong. Try again later.</span>
    }
    if (this.state.anns.length) {
      annHtml = this.state.anns.map((value, i) => (
        <DispalyAnnouncement value={value} key={i} />
      ));
    }
    return annHtml;
  }
}
export default Announcements;
