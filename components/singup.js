import React from "react";
import axios from "axios";
import validator from "validator";
import Country from "./utilities/selectCountry";
import Link from "next/link";
class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      fname: "",
      lname: "",
      birth_year: "",
      gender: "",
      country_iso: "",
      permit_no: "",
      permit_type: "",
      exp_date: "",
      refugee_center: "",
      ts_cs: false,
      phone: "",
      email: "",
      error: {
        //these values will be typed by user and need validation
        email: "",
        phone: "",
        permit_no: "",
        exp_date: "",
        fname: "",
        lname: "",
        ts_cs: "",
      },
      submitError: "",
      success: false,
    };
    this.state = this.initialState;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.isvalid = this.isvalid.bind(this);
    this.setDisplayError = this.setDisplayError.bind(this);
    this.displayError = this.displayError.bind(this);
    this.clearError = this.clearError.bind(this);
    this.isAvailable = this.isAvailable.bind(this);
  }

  setDisplayError = (name) => {
    var err = this.state.error;
    if (err.hasOwnProperty(name)) {
      err[name] = this.errorMsgs.hasOwnProperty(name)
        ? this.errorMsgs[name]
        : "";
      this.setState({ error: err });
    }
  };

  displayError = (name) => {
    var err = this.state.error;
    if (err.hasOwnProperty(name)) {
      err[name];
      return <span className="text-danger pl-1 small alert">{err[name]}</span>;
    }
  };

  clearError = (name) => {
    var err = this.state.error;
    if (err.hasOwnProperty(name)) {
      if (!validator.isEmpty(err[name])) {
        err[name] = "";
        this.setState({ error: err });
      }
    }
  };

  isAvailable = (field, val) => {
    let API_URL = process.env.API_URL;
    axios
      .post(`${API_URL}/user/isavailable`, { field: field, value: val })
      .then((res) => {
        if (!res.data) {
          if (this.errorMsgs.hasOwnProperty(field)) {
            this.errorMsgs[field] = "This has been registered here already.";
            this.setDisplayError(field);
            Object.assign(this.errorMsgs, this.initialErrors);
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  initialErrors = {
    email: "Email format is incorrect",
    phone: "Phone number invalid for SA.",
    permit_no: "Permit number invalid.",
  };
  errorMsgs = {
    email: "Email format is incorrect",
    phone: "Phone number invalid for SA.",
    permit_no: "Permit number invalid.",
    exp_date: "Only permits expiring from 01/01/2020 are allowed.",
    fname: "Please use the first name on your permit with letters only.",
    lname: "Please use the surname on your permit with letters only.",
    ts_cs: "(Please agree to  the terms and conditions.)",
  };

  isvalid = (e) => {
    var name = e.target.name;
    var value =
      e.target.type === "checkbox" ? e.target.checked : e.target.value;
    this.clearError(name);
    //if (validator.isBoolean(value) && value === true) return;
    if (e.target.type !== "checkbox" && validator.isEmpty(value)) return; //isEmpty function expect strings
    if (name === "email") {
      if (!validator.isEmail(value)) this.setDisplayError("email");
      else this.isAvailable(name, value);
    } else if (name === "phone") {
      if (!validator.isMobilePhone(value, "en-ZA")) this.setDisplayError(name);
      else this.isAvailable(name, value);
    } else if (name === "permit_no") {
      if (!validator.isLength(value, { min: 1, max: 16 }))
        this.setDisplayError(name);
      else this.isAvailable(name, value);
    } else if (name === "fname" && !validator.isAlpha(value)) {
      this.setDisplayError(name);
    } else if (name === "lname" && !validator.isAlpha(value)) {
      this.setDisplayError(name);
    } else if (name === "exp_date" && !validator.isAfter(value, "2020-01-01")) {
      this.setDisplayError(name);
    } else if (name === "ts_cs" && value === false) {
      this.setDisplayError(name);
    }
    if (!validator.isEmpty(this.state.submitError)) {
      this.setState({ submitError: "" });
    }
  };

  handleChange = (e) => {
    var target = e.target;
    var name = target.name;
    var value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [name]: value }, this.isvalid(e));
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let dataObj = this.state;
    var valRequired = [
      "fname",
      "lname",
      "gender",
      "birth_year",
      "country_iso",
      "permit_type",
      "permit_no",
      "exp_date",
      "phone",
      "refugee_center",
      "ts_cs",
    ];
    let errorKeys = Object.keys(dataObj.error);
    errorKeys.forEach((v) => {
      if (!validator.isEmpty(dataObj.error[v])) {
        this.setState({
          submitError: "Please enter all required fields",
        });
        return;
      }
    });
    for (var i = 0; i < valRequired.length; i++) {
      if (valRequired[i] === "ts_cs") {
        if (dataObj[valRequired[i]] === false) {
          this.setState({
            submitError: "Please agree to the terms and conditions",
          });
          return;
        }
      } else if (validator.isEmpty(dataObj[valRequired[i]])) {
        this.setState({
          submitError: "Please enter all required fields",
        });
        return;
      }
    }
    if (this.state.submitError) return;
    let API_URL = process.env.API_URL;
    axios
      .post(`${API_URL}/user/signup`, dataObj)
      .then((user) => {
        if (user.data) {
          var phone = this.state.phone;
          this.setState(this.initialState);
          this.setState({
            success: "Congratulations! You have joined successfully!",
            phone: phone,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  signUpForm = () => {
    var asterisk = function () {
      return <span className="text-danger">*</span>;
    };
    return (
      <form id="signup">
        <div className="row mb-1">
          <div className="col-md-6">
            <label htmlFor="firstname">
             <i className='fa fa-user'></i> First Name {asterisk()} :{this.displayError("fname")}
            </label>
            <input
              type="text"
              name="fname"
              value={this.state.fname}
              onChange={this.handleChange}
              className="form-control"
              required={true}
              id="firstname"
              maxLength="50"
            />
          </div>

          <div className="col-md-6">
            <label htmlFor="lastname">
              Last Name {asterisk()} : {this.displayError("lname")}
            </label>
            <input
              type="text"
              name="lname"
              value={this.state.lname}
              onChange={this.handleChange}
              className="form-control"
              required={true}
              id="lastname"
              maxLength="50"
            />
          </div>
        </div>
        <div className="row mb-1">
          <div className="col-md-4">
            <label htmlFor="gender">Gender {asterisk()} : </label>
            <select
              name="gender"
              value={this.state.gender}
              onChange={this.handleChange}
              className="form-control"
              required={true}
              id="gender"
            >
              <option>Select</option>
              <option value="f">Female</option>
              <option value="m">Male</option>
            </select>
          </div>
          <div className="col-md-4">
            <label htmlFor="birth_year">Year of Birth {asterisk()} : </label>
            <select
              name="birth_year"
              value={this.state.birth_year}
              onChange={this.handleChange}
              required={true}
              className="form-control"
              id="birth_year"
            >
              <option>Select</option>
              <option value="1950-1960">1950-1960</option>
              <option value="1960-1970">1961-1970</option>
              <option value="1971-1980">1971-1980</option>
              <option value="1981-1990">1981-1990</option>
              <option value="1991-2000">1991-2000</option>
              <option value="2001-2020">2001-2020</option>
            </select>
          </div>
          <div className="col-md-4">
            <label htmlFor="country">Country of origin {asterisk()} :</label>
            <Country state={this.state} handleChange={this.handleChange} />
          </div>
        </div>
        <div className="row mb-1">
          <div className="col-md-6">
            <label htmlFor="permit_type">Permit Type {asterisk()} :</label>
            <select
              name="permit_type"
              value={this.state.permit_type}
              onChange={this.handleChange}
              className="form-control"
              id="permit_type"
              required={true}
            >
              <option>Select</option>
              <option value="status">Status</option>
              <option value="asylum">Asylum</option>
            </select>
          </div>
          <div className="col-md-6">
            <label htmlFor="permit_no">
              Permit No {asterisk()} : {this.displayError("permit_no")}
            </label>
            <input
              type="text"
              name="permit_no"
              value={this.state.permit_no}
              onChange={this.handleChange}
              className="form-control"
              id="permit_no"
              required={true}
            />
          </div>
        </div>
        <div className="row mb-1">
          <div className="col-md-6">
            <label htmlFor="exp_date">
              Expiration Date {asterisk()} : {this.displayError("exp_date")}
            </label>
            <input
              type="date"
              name="exp_date"
              value={this.state.exp_date}
              onChange={this.handleChange}
              className="form-control"
              id="exp_date"
              required={true}
            />
          </div>
          <div className="col-md-6">
            <label htmlFor="phone">
              <i className='fa fa-mobile'></i> Phone No {asterisk()} : {this.displayError("phone")}
            </label>
            <input
              type="number"
              className="form-control"
              name="phone"
              value={this.state.phone}
              onChange={this.handleChange}
              id="phone"
              required={true}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="center">Refugee Centre {asterisk()} :</label>
            <select
              name="refugee_center"
              value={this.state.refugee_center}
              onChange={this.handleChange}
              className="form-control"
              id="center"
              required={true}
            >
              <option>Select</option>
              <option value="Cape Town">Cape Town</option>
              <option value="Durban">Durban</option>
              <option value="Musina">Musina</option>
              <option value="Pretoria">Pretoria</option>
              <option value="Port Elizabeth">Port Elizabeth</option>
            </select>
          </div>
          <div className="col-md-6">
            <label htmlFor="email">
              Email Address (optional): {this.displayError("email")}
            </label>
            <input
              type="email"
              name="email"
              value={this.state.email}
              onChange={this.handleChange}
              id="email"
              className="form-control"
              maxLength="50"
            />
          </div>
        </div>
        <div className="row mt-1">
          <div className="col">
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                name="ts_cs"
                value={this.state.ts_cs}
                onChange={this.handleChange}
                id="ts_cs"
                required={true}
              />
              <label className="form-check-label" htmlFor="ts_cs">
                I accept our{" "}
                <Link href="/terms">
                  <a>Terms</a>
                </Link>{" "}
                and{" "}
                <Link href="/privacy">
                  <a>Privacy Policy</a>
                </Link>
                {this.displayError("ts_cs")}
              </label>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 mt-1 mb-3">
            <div className="text-danger text-center small">
              {this.state.submitError}
            </div>
            <button
              type="button"
              onClick={this.handleSubmit}
              className="btn btn-block btn-outline-info"
            >
              Join
            </button>
          </div>
        </div>
      </form>
    );
  };
  render() {
    if (this.state.success) {
      return (
        <div className="card text-center">
          <div className="card-body">
            <h1>{this.state.success}</h1>
            <p>You will shortly receive an sms to your phone number below.</p>
            <div className="alert alert-warning font-weight-bold">
              {this.state.phone}
            </div>
            <p>Please click the link in the SMS to confirm your number.</p>
            <p>
              You will then be able to receive updates on your appointments or
              any urgent information.
            </p>
            <p>
              Your information can be updated{" "}
              <Link href="#">
                <a>here</a>
              </Link>{" "}
              at anytime.
            </p>
            <p className="btn btn-success bg-sucess font-weight-bold">
              We appreciate your time!
            </p>
          </div>
        </div>
      );
    }
    var form = this.signUpForm();
    return form;
  }
}
export default SignUp;
