import React from "react";
import axios from "axios";
import parseHtml from 'html-react-parser';
class Announcement extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ann: "", error: 0 };
  }
  componentDidMount() {
    let API_URL = process.env.API_URL;
    axios
      .get(`${API_URL}/content/announcement`)
      .then((ann) => {
        if (ann) {
          this.setState({ ann: ann.data[0] });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentWillUnmount() {}

  render() {
    if (!this.state.ann) {
      return <div />;
    }
   /*  var bgColors = ['bg-danger','bg-info','bg-success','bg-primary'];
    var bgCls = bgColors[Math.floor(Math.random() * bgColors.length)]; */
    return (
      <div className="card mt-2 mb-2 latest">
        <div className={"card-header bg-success text-white"}>{parseHtml(this.state.ann.title)}</div>
        <div className="card-body">
          {parseHtml(this.state.ann.text)}
        </div>
      </div>
    );
  }
}

export default Announcement;
